package com.ztesoft.ip.entity;

import java.util.List;

public class BaiduMap {
	private String status;
	private String message;
	private String total;
	private List<BaiduMapDetail> results;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<BaiduMapDetail> getResults() {
		return results;
	}

	public void setResults(List<BaiduMapDetail> results) {
		this.results = results;
	}

}
